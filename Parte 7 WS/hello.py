#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul  5 17:10:43 2021

@author: usrsiac
"""
import json
from flask import Flask, render_template, request, url_for, flash, redirect
from flask_cors import CORS

app = Flask(__name__)

CORS(app)

#@app.route('/')
#@app.route('/generalArticulos')
def generalArticulos():
    from py2neo import Graph
    graph = Graph("bolt://localhost:7687", auth=("neo4j", "123456789"))
    #Cargamos los esquemas de medicina y vivo
    consulta=graph.run('match(i:instancia)-[:rdfs__subClassOf]->(titulo:title)'+
                       'match(i:instancia)-[:rdfs__subClassOf]->(abstract:abstract)'+
                       'match(i:instancia)-[:rdfs__subClassOf]->(link:link)'+
                       'match(i:instancia)-[:rdfs__subClassOf]->(fuente:fuente)'+
                       'match(i:instancia)-[:rdfs__subClassOf]->(autores:autor)'+
                      'return titulo,abstract,link,fuente,autores,autores.nombres').to_data_frame();
    

    
    dict_autor_persona={}
    tam=len(consulta['autores.nombres'])
    
    for i in range(tam):
        con_autor=graph.run('match(i:autor{nombres:"'+consulta['autores.nombres'][i]+'"})-[:rdfs__subClassOf]->(p:persona) return i,p.nombre').to_data_frame();
        listautor=[]
        for j in con_autor.index:
            listautor.append(con_autor['p.nombre'][j])
        dict_autor_persona[i]=listautor
    
    import pandas as pd
    
    df = pd.DataFrame([[key, dict_autor_persona[key]] for key in dict_autor_persona.keys()], columns=['nodo', 'autores'])
    consulta['autores']=df['autores']
    
    return consulta.to_json(orient = 'index')

#ingresa y devuelve el resultado 
@app.route('/consulta', methods=('GET', 'POST'))
def create():
    if request.method == 'POST':
        title = request.json['busqueda']
        repeticiones=int(request.json['cantidad'])
        dicprueba={}
        for i in range(repeticiones):
            dicprueba[i]=title
        json_data = json.dumps(dicprueba,indent=0)
    return json_data

##################################################################################
#Web Services generados
 #################################################################################

@app.route('/generalArticulos')
def generalarticulos():
    def consultaGeneral ():
        from py2neo import Graph
        graph = Graph("bolt://localhost:7687", auth=("neo4j", "123456789"))
    
        #Cargamos los esquemas de medicina y vivo
        consulta=graph.run('match(i:instancia)-[:rdfs__subClassOf]->(titulo:title)'+
                           'match(i:instancia)-[:rdfs__subClassOf]->(abstract:abstract)'+
                           'match(i:instancia)-[:rdfs__subClassOf]->(link:link)'+
                           'match(i:instancia)-[:rdfs__subClassOf]->(fuente:fuente)'+
                           'match(i:instancia)-[:rdfs__subClassOf]->(autores:autor)'+
                           'match(i:instancia)-[:rdfs__subClassOf]->(keyword:keyword)'+
                           'match(i:instancia)-[:rdfs__subClassOf]->(tituloCategoria:categoriaTitulo)'+
                           'match(i:instancia)-[:rdfs__subClassOf]->(Abstractcategoria:categoriaAbstract)'+ 
                          'return titulo,abstract,link,fuente,autores,autores.nombres,keyword.palabras,tituloCategoria.caracteristicas,Abstractcategoria.caracteristicas').to_data_frame();
        return consulta
    
    def obtenerautores(caracteristica):
        from py2neo import Graph
        graph = Graph("bolt://localhost:7687", auth=("neo4j", "123456789"))
        
        dict_autor_persona={}
        tam=len(caracteristica)
        for i in range(tam):
            con_autor=graph.run('match(i:autor{nombres:"'+caracteristica[i]+'"})-[:rdfs__subClassOf]->(p:persona) return i,p.nombre').to_data_frame();
            listautor=[]
            for j in con_autor.index:
                listautor.append(con_autor['p.nombre'][j])
            dict_autor_persona[i]=listautor
    
        import pandas as pd
    
        df = pd.DataFrame([[key, dict_autor_persona[key]] for key in dict_autor_persona.keys()], columns=['nodo', 'autores'])
        consulta['autores']=df['autores']

    def obtenerkeywords(caracteristica):
        from py2neo import Graph
        graph = Graph("bolt://localhost:7687", auth=("neo4j", "123456789"))
        
        dict_autor_persona={}
        tam=len(caracteristica)
        for i in range(tam):
            con_autor=graph.run('match(i:keyword{palabras:"'+caracteristica[i]+'"})-[:rdfs__subClassOf]->(p:palabra) return i,p.nombre').to_data_frame();
            listautor=[]
            for j in con_autor.index:
                listautor.append(con_autor['p.nombre'][j])
            dict_autor_persona[i]=listautor
    
        import pandas as pd
    
        df = pd.DataFrame([[key, dict_autor_persona[key]] for key in dict_autor_persona.keys()], columns=['nodo', 'keywords'])
        consulta['keywords']=df['keywords'] 

    def obtenerCategoria(caracteristica):
        from py2neo import Graph
        graph = Graph("bolt://localhost:7687", auth=("neo4j", "123456789"))
        
        dict_autor_persona={}
        tam=len(caracteristica)
        for i in range(tam):
            con_autor=graph.run('match(i:categoriaTitulo{caracteristicas:"'+caracteristica[i]+'"})-[:rdfs__subClassOf]->(p:palabraclave) return i,p.nombre').to_data_frame();
            listautor=[]
            for j in con_autor.index:
                listautor.append(con_autor['p.nombre'][j])
            dict_autor_persona[i]=listautor
    
        import pandas as pd
    
        df = pd.DataFrame([[key, dict_autor_persona[key]] for key in dict_autor_persona.keys()], columns=['nodo', 'categoriaTitulo'])
        consulta['categoriaTitulo']=df['categoriaTitulo']
    
    def obtenerCategoriaAbstract(caracteristica):
        from py2neo import Graph
        graph = Graph("bolt://localhost:7687", auth=("neo4j", "123456789"))
        
        dict_autor_persona={}
        tam=len(caracteristica)
        for i in range(tam):
            con_autor=graph.run('match(i:categoriaAbstract{caracteristicas:"'+caracteristica[i]+'"})-[:rdfs__subClassOf]->(p:palabraclave) return i,p.nombre').to_data_frame();
            listautor=[]
            for j in con_autor.index:
                listautor.append(con_autor['p.nombre'][j])
            dict_autor_persona[i]=listautor
    
        import pandas as pd
    
        df = pd.DataFrame([[key, dict_autor_persona[key]] for key in dict_autor_persona.keys()], columns=['nodo', 'categoriaAbstract'])
        consulta['categoriaAbstract']=df['categoriaAbstract']

    consulta=consultaGeneral()
    obtenerautores(consulta['autores.nombres'])
    #obtenerkeywords(consulta['keyword.palabras'])
    #obtenerCategoria(consulta['tituloCategoria.caracteristicas'])
    #obtenerCategoriaAbstract(consulta['Abstractcategoria.caracteristicas'])
    response=consulta.to_json(orient = 'records')
    #response.headers.add("Access-Control-Allow-Origin", "*")

    return response


@app.route('/nodeSimilarity', methods=('GET', 'POST'))
def recomendacionNodeSimilarity():
    import pandas as pd
    import random
    import spacy
    from spacy import displacy
    from spacy.lang.en.stop_words import STOP_WORDS
    nlp = spacy.load('en')
    
    def recomNodesimilarity(data):
        from py2neo import Graph
        graph = Graph("bolt://localhost:7687", auth=("neo4j", "123456789")) 
        list_resultados=[]
        for i in range(int(len(data['caracteristicasT1'])/10)):
            list_resultados.append(graph.run("match(n:categoriaTitulo{caracteristicas:'"+data['caracteristicasT1'][i]+"'})<-[:rdfs__subClassOf]-(i:instancia)-[:rdfs__subClassOf]->(titulo:title)"+
            "match(i:instancia)-[:rdfs__subClassOf]->(abstract:abstract)"+
            "match(i:instancia)-[:rdfs__subClassOf]->(link:link)"+
            "match(i:instancia)-[:rdfs__subClassOf]->(autor:autor)"+
            "match(i:instancia)-[:rdfs__subClassOf]->(fuente:fuente)"+
            "match(i:instancia)-[:rdfs__subClassOf]->(key:key)"+
            "return titulo,abstract,link,autor,fuente,key").to_data_frame())
        result = pd.concat(list_resultados)  
        result.reset_index(drop=True, inplace=True)
        json=result.to_json(orient = 'records')
        return json
           
    def nodesimilarity(listpln):
        from py2neo import Graph
        #Recorrido dentro de la base 
        articulos_obtenidos=[]
        aux_diccionario={}
    
        graph = Graph("bolt://localhost:7687", auth=("neo4j", "123456789"))
    
        for i in range (len(listpln)):
            #print(listpln[i])
            try:
                coincidencia=graph.run('match(n:palabraclave{nombre:"'+listpln[i]+'"})<-[:rdfs__subClassOf]-(u:categoriaTitulo)<-[:rdfs__subClassOf]-(i:instancia)-[:rdfs__subClassOf]->(t:title) return n,u,i,id(t),t').to_data_frame()
                #print("Articulos que contienen:",listpln[i])
                #print(list(coincidencia.get("id(t)")))
                aux_pesos=list(coincidencia.get("id(t)"))
                #Este for permite guardar todos lo id de los articulos relacionados con esa palabra
                for i in range(len(aux_pesos)):
                    articulos_obtenidos.append(list(coincidencia.get("id(t)"))[i])
                    #print(list(coincidencia.get("t"))[i])
                    aux_diccionario[list(coincidencia.get("id(t)"))[i]]=list(coincidencia.get("t"))[i]
                #print("-------------------------")
            except:
                print("No se encontro en la base la palabra ",listpln[i])
    
    
    
        limpieza=list(set(articulos_obtenidos))
        #print(limpieza)
        
    
        resultados={}
        for i in range(len(limpieza)):
            resultado=graph.run('match(n)<-[x:rdfs__subClassOf]-(i:instancia)-[x1:rdfs__subClassOf]->(k:categoriaTitulo) where ID(n)='+str(limpieza[i])+' return n.title,k.caracteristicas').to_data_frame()
            resultados[resultado['k.caracteristicas'][0]]=resultado['n.title'][0]
        #print(resultados.keys())
        
        import pandas as pd
    
        df1 = pd.read_csv('nodesimilarity.csv')
    

        #nodesimilarity=graph.run("CALL gds.nodeSimilarity.stream('myGraph20') "+
        #                         "YIELD node1, node2, similarity "+
        #                         "RETURN gds.util.asNode(node1).caracteristicas  AS caracteristicasT1, gds.util.asNode(node2).caracteristicas  AS caracteristicasT2, similarity "+
        #                         "ORDER BY similarity DESCENDING, caracteristicasT1, caracteristicasT2").to_data_frame()
    
        nodesimilarity=df1    
    
    
        import pandas as pd
    
        df = pd.DataFrame()
        list_keys=list(resultados.keys())
        for i in range(len(list_keys)):
            filtrado=nodesimilarity[nodesimilarity['caracteristicasT1'] == list_keys[i]]
            df =pd.concat([df,filtrado])
    
        df_orden = df.sort_values('similarity',ascending=False)
        df_orden.head()
        #eliminamos lo repetidos
        dataset=df_orden.drop_duplicates(subset=['caracteristicasT1'])  
        #limpiamos los indices
        dataset=dataset.reset_index()
        return dataset
        
    def PLN(texto):
        texto=texto.lower()
        doc = nlp(texto)
        list_importante=[]
        for token in doc:
            #print(token.text, token.lemma_, token.pos_, token.tag_, token.dep_,token.shape_, token.is_alpha, token.is_stop)
            if token.pos_=='VERB' or token.pos_=='INTJ' or token.pos_=="ADJ" or token.pos_=="NOUN" or token.pos_=="ADV":
                list_importante.append(token.lemma_)
        #Funcion que permite ordenar el list en forma del abecedario.
        list_importante.sort()
        #print("Palabras a buscar",importante)
        return list_importante
    
    if request.method == 'POST':
        texto = request.json['busqueda']
        list_pln=PLN(texto)
        resultado=nodesimilarity(list_pln)
        salida=recomNodesimilarity(resultado)
    else:
        salida='Es un post'
    
    return salida


@app.route('/degreecentrality', methods=('GET', 'POST'))
def degreecentrality ():
    import pandas as pd
    import random
    import spacy
    from spacy import displacy
    from spacy.lang.en.stop_words import STOP_WORDS
    nlp = spacy.load('en')


    def recomDegreeSimilarity(data):
        from py2neo import Graph
        graph = Graph("bolt://localhost:7687", auth=("neo4j", "123456789")) 
        list_resultados=[]
        for i in range(int(len(data['name'])/10)):
            list_resultados.append(graph.run("match(n:categoriaTitulo{caracteristicas:'"+data['name'][i]+"'})<-[:rdfs__subClassOf]-(i:instancia)-[:rdfs__subClassOf]->(titulo:title)"+
            "match(i:instancia)-[:rdfs__subClassOf]->(abstract:abstract)"+
            "match(i:instancia)-[:rdfs__subClassOf]->(link:link)"+
            "match(i:instancia)-[:rdfs__subClassOf]->(autor:autor)"+
            "match(i:instancia)-[:rdfs__subClassOf]->(fuente:fuente)"+
            "match(i:instancia)-[:rdfs__subClassOf]->(key:key)"+
            "return titulo,abstract,link,autor,fuente,key").to_data_frame())
        result = pd.concat(list_resultados)  
        result.reset_index(drop=True, inplace=True)
        json=result.to_json(orient = 'records')
        print(json)
        return json
    
    def degrecentrality(listpln):
        from py2neo import Graph
        graph = Graph("bolt://localhost:7687", auth=("neo4j", "123456789"))
        
        articulos_obtenidos=[]
        aux_diccionario={}
    
        for i in range (len(listpln)):
            #print(listpln[i])
            try:
                coincidencia=graph.run('match(n:palabraclave{nombre:"'+listpln[i]+'"})<-[:rdfs__subClassOf]-(u:categoriaTitulo)<-[:rdfs__subClassOf]-(i:instancia)-[:rdfs__subClassOf]->(t:title) return n,u,i,id(t),t').to_data_frame()
                aux_pesos=list(coincidencia.get("id(t)"))
                #Este for permite guardar todos lo id de los articulos relacionados con esa palabra
                for i in range(len(aux_pesos)):
                    articulos_obtenidos.append(list(coincidencia.get("id(t)"))[i])
                    aux_diccionario[list(coincidencia.get("id(t)"))[i]]=list(coincidencia.get("t"))[i]
            except:
                print("No se encontro en la base la palabra ",listpln[i])
        limpieza=list(set(articulos_obtenidos))
        #print(limpieza)
        resultados={}
        for i in range(len(limpieza)):
            resultado=graph.run('match(n)<-[x:rdfs__subClassOf]-(i:instancia)-[x1:rdfs__subClassOf]->(k:categoriaTitulo) where ID(n)='+str(limpieza[i])+' return n.title,k.caracteristicas').to_data_frame()
            resultados[resultado['k.caracteristicas'][0]]=resultado['n.title'][0]
        #print('Categorias Encotradas')
        #print(resultados.keys())
    
        import pandas as pd
    
        df1 = pd.read_csv('degree.csv')
    
        #degreecentrality=graph.run("CALL gds.degree.stream('myGraph12') "+
        #                         "YIELD nodeId, score "+
        #                         "RETURN gds.util.asNode(nodeId).caracteristicas AS name, score AS followers "+
        #                         "ORDER BY followers DESC, name DESC").to_data_frame()
        
        degreecentrality=df1
       
    
        import pandas as pd
        df = pd.DataFrame()
        list_keys=list(resultados.keys())
        for i in range(len(list_keys)):
            filtrado=degreecentrality[degreecentrality['name'] == list_keys[i]]
            df =pd.concat([df,filtrado])
    
        df_orden = df.sort_values('followers',ascending=False)
        df_orden.head()
        #eliminamos lo repetidos
        #dataset=df_orden.drop_duplicates(subset=['caracteristicasT1'])  
        #limpiamos los indices
        dataset=df_orden.reset_index()
        dataset=dataset.reset_index()
        return dataset    

    def PLN(texto):
            texto=texto.lower()
            doc = nlp(texto)
            list_importante=[]
            for token in doc:
                #print(token.text, token.lemma_, token.pos_, token.tag_, token.dep_,token.shape_, token.is_alpha, token.is_stop)
                if token.pos_=='VERB' or token.pos_=='INTJ' or token.pos_=="ADJ" or token.pos_=="NOUN" or token.pos_=="ADV":
                    list_importante.append(token.lemma_)
            #Funcion que permite ordenar el list en forma del abecedario.
            list_importante.sort()
            print(list_importante)
            return list_importante

    if request.method == 'POST':
            texto = request.json['busqueda']
            list_pln=PLN(texto)
            resultado=degrecentrality(list_pln)
            salida=recomDegreeSimilarity(resultado)
    else:
            salida='Es un post'
    return salida
    


@app.route('/betwenscentrality', methods=('GET', 'POST'))
def betwenscentrality():
    import pandas as pd
    import random
    import spacy
    from spacy import displacy
    from spacy.lang.en.stop_words import STOP_WORDS
    nlp = spacy.load('en')

        
    def recombetweennesCentrality(data):
        
        from py2neo import Graph
        graph = Graph("bolt://localhost:7687", auth=("neo4j", "123456789")) 
        list_resultados=[]
        for i in range(int(len(data['caracteristicas'])/10)):
            list_resultados.append(graph.run("match(n:categoriaTitulo{caracteristicas:'"+data['caracteristicas'][i]+"'})<-[:rdfs__subClassOf]-(i:instancia)-[:rdfs__subClassOf]->(titulo:title)"+
            "match(i:instancia)-[:rdfs__subClassOf]->(abstract:abstract)"+
            "match(i:instancia)-[:rdfs__subClassOf]->(link:link)"+
            "match(i:instancia)-[:rdfs__subClassOf]->(autor:autor)"+
            "match(i:instancia)-[:rdfs__subClassOf]->(fuente:fuente)"+
            "match(i:instancia)-[:rdfs__subClassOf]->(key:key)"+
            "return titulo,abstract,link,autor,fuente,key").to_data_frame())
        result = pd.concat(list_resultados)  
        result.reset_index(drop=True, inplace=True)
        json=result.to_json(orient = 'records')
        #print(json)
        return json   
    def betweennescentrality(listpln):
        from py2neo import Graph
        graph = Graph("bolt://localhost:7687", auth=("neo4j", "123456789"))
        
        articulos_obtenidos=[]
        aux_diccionario={}
    
        for i in range (len(listpln)):
            #print(listpln[i])
            try:
                coincidencia=graph.run('match(n:palabraclave{nombre:"'+listpln[i]+'"})<-[:rdfs__subClassOf]-(u:categoriaTitulo)<-[:rdfs__subClassOf]-(i:instancia)-[:rdfs__subClassOf]->(t:title) return n,u,i,id(t),t').to_data_frame()
                aux_pesos=list(coincidencia.get("id(t)"))
                #Este for permite guardar todos lo id de los articulos relacionados con esa palabra
                for i in range(len(aux_pesos)):
                    articulos_obtenidos.append(list(coincidencia.get("id(t)"))[i])
                    aux_diccionario[list(coincidencia.get("id(t)"))[i]]=list(coincidencia.get("t"))[i]
            except:
                print("No se encontro en la base la palabra ",listpln[i])
        
        limpieza=list(set(articulos_obtenidos))
        #print(limpieza)
        resultados={}
        for i in range(len(limpieza)):
            resultado=graph.run('match(n)<-[x:rdfs__subClassOf]-(i:instancia)-[x1:rdfs__subClassOf]->(k:categoriaTitulo) where ID(n)='+str(limpieza[i])+' return n.title,k.caracteristicas').to_data_frame()
            resultados[resultado['k.caracteristicas'][0]]=resultado['n.title'][0]
        #print('Categorias Encotradas')
        #print(resultados.keys())
    
        import pandas as pd
        df1 = pd.read_csv('betweens.csv')
        
        #betweenesscentrality=graph.run("CALL gds.betweenness.stream('myGraph15') "+
        #                         "YIELD nodeId, score "+
        #                         "RETURN gds.util.asNode(nodeId).caracteristicas AS caracteristicas, score "+
        #                         "ORDER BY caracteristicas ASC").to_data_frame()
        
        
        betweenesscentrality=df1
        
        import pandas as pd
        df = pd.DataFrame()
        list_keys=list(resultados.keys())
        for i in range(len(list_keys)):
            filtrado=betweenesscentrality[betweenesscentrality['caracteristicas'] == list_keys[i]]
            df =pd.concat([df,filtrado])
    
        df_orden = df.sort_values('score',ascending=False)
        df_orden.head()
        
        dataset=df_orden.reset_index()
        dataset=dataset.reset_index()
        #print(dataset)
    
        
        return dataset
    
    def PLN(texto):
        texto=texto.lower()
        doc = nlp(texto)
        list_importante=[]
        for token in doc:
            #print(token.text, token.lemma_, token.pos_, token.tag_, token.dep_,token.shape_, token.is_alpha, token.is_stop)
            if token.pos_=='VERB' or token.pos_=='INTJ' or token.pos_=="ADJ" or token.pos_=="NOUN" or token.pos_=="ADV":
                list_importante.append(token.text)
        #Funcion que permite ordenar el list en forma del abecedario.
        list_importante.sort()
        print(list_importante)
        return list_importante
    
    if request.method == 'POST':
            texto = request.json['busqueda']
            list_pln=PLN(texto)
            resultado=betweennescentrality(list_pln)
            salida=recombetweennesCentrality(resultado)
    else:
            salida='Es un post'
    return salida
    
    
@app.route('/relaciones', methods=('GET', 'POST'))
def relaciones():
    import pandas as pd
    
    def kddCategoria(texto):
        from py2neo import Graph
        graph = Graph("bolt://localhost:7687", auth=("neo4j", "123456789")) 
        result=graph.run("""match(n:owl__Class{rdfs__label:'"""+texto+"""'})-[r:rdfs__subClassOf]->(p:owl__Class) return n, r, p,ID(n),ID(p) UNION match(n:owl__Class{rdfs__label:'"""+texto+"""'})-[r:rdfs__subClassOf]-(p:owl__Class) return  n, r, p,ID(n),ID(p)""").to_data_frame()
        
        listdirec=[]
        for i in range(len(result["n"])):
            direccion=str(result["r"][i]).find(str(result["ID(n)"][i]))
            if direccion>=22:
                listdirec.append([result["n"][i]["rdfs__label"],str(result["ID(n)"][i]),"padre",result["p"][i]["rdfs__label"],str(result["ID(p)"][i])])
            else:
                listdirec.append([result["n"][i]["rdfs__label"],str(result["ID(n)"][i]),"hijo",result["p"][i]["rdfs__label"],str(result["ID(p)"][i])])
        
        df = pd.DataFrame.from_records(listdirec,columns=['name1','id1', 'relacion', 'name2','id2'])
        
        json=df.to_json(orient = 'records')
        
        return json
    
    texto = request.json['busqueda']
    rest=kddCategoria(texto)
    return rest